# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.2] - Feb 14, 2018
### Changed
- DateTime objects returned by the generator are now timezone-aware

## [0.3.1] - Feb. 12, 2018
### Changed
- Return field default when appropriate generator not found

## [0.3.0] - Feb. 12, 2018
### Changed
- Switch to `Faker` dependency from `fake_factory`
- Update generators to use the new `snake_case` casing
- Project name change for inclusion back into PyPi

## [0.2] - Jan. 23, 2013
### Added
- Requirements.txt
- Fake browser preview

## [0.1] - Dec. 01, 2012
### Added
- Django Model instance generator
- Django template tag and filter

[0.3.1]: https://gitlab.com/solarliner/django-populate/compre/0.3.0...v0.3.1
[0.3.0]: https://gitlab.com/solarliner/django-populate/compre/v0.2...0.3.0
[0.2]: https://gitlab.com/solarliner/django-populate/compare/v0.1...v0.2
[0.1]: https://gitlab.com/solarliner/django-populate/tags/v0.1
