"""

Django-faker uses python-faker to generate test data for Django models and templates.

"""

__version__ = '0.3.2'


class Populator(object):
    instance = None
    populators = {}
    generators = {}

    @classmethod
    def __new__(cls, *args, **kwargs):
        if cls.instance is None:
            cls.instance = super(Populator, cls).__new__(*args, **kwargs)
        return cls.instance

    def __init__(self):
        #        assert False, "Cannot create a instance of Faker"
        pass

    @staticmethod
    def get_codename(locale=None, providers=None):
        """
        codename = locale[-Provider]*
        """
        from django.conf import settings
        # language
        locale = locale or getattr(settings, 'FAKER_LOCALE', getattr(settings, 'LANGUAGE_CODE', None))
        # providers
        providers = providers or getattr(settings, 'FAKER_PROVIDERS', None)

        codename = locale or 'default'

        if providers:
            codename += "-" + "-".join(sorted(providers))

        return codename

    @classmethod
    def get_generator(cls, locale=None, providers=None, codename=None):
        """
        use a codename to cache generators
        """

        codename = codename or cls.get_codename(locale, providers)

        if codename not in cls.generators:
            from faker import Faker as FakerGenerator
            # initialize with faker.generator.Generator instance
            # and remember in cache
            cls.generators[codename] = FakerGenerator(locale, providers)
            cls.generators[codename].seed(cls.generators[codename].random_int())

        return cls.generators[codename]

    @classmethod
    def get_populator(cls, locale=None, providers=None):
        """

        uses:

            from django_populate import Faker
            pop = Faker.getPopulator()

            from myapp import models
            pop.addEntity(models.MyModel, 10)
            pop.addEntity(models.MyOtherModel, 10)
            pop.execute()

            pop = Faker.getPopulator('it_IT')

            pop.addEntity(models.MyModel, 10)
            pop.addEntity(models.MyOtherModel, 10)
            pop.execute()

        """

        codename = cls.get_codename(locale, providers)

        if codename not in cls.populators:
            generator = cls.generators.get(codename, None) or cls.get_generator(codename=codename)

            from django_populate import populator

            cls.populators[codename] = populator.Populator(generator)

        return cls.populators[codename]


#        if not cls.populator:
#            cls.populator= populators.Populator(
#            # initialize with faker.generator.Generator instance
#            FakerGenerator(
#
#                getattr(settings,'FAKER_LOCALE', getattr(settings,'LANGUAGE_CODE', locale)),
#
#                getattr(settings,'FAKER_PROVIDERS', providers)
#            )
#        )

Faker = Populator
