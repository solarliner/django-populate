import random

from django.db.models import ForeignKey, ManyToManyField, OneToOneField
from django.db.models.fields import *
from django.db.models.fields.files import *
from django.utils.timezone import get_current_timezone

from django_populate.guessers import Name


class FieldTypeGuesser(object):

    def __init__(self, generator):
        """
        :param generator: Generator
        """
        self.generator = generator

    def guess_format(self, field):

        generator = self.generator
        if isinstance(field, BooleanField):
            return lambda x: generator.boolean()
        if isinstance(field, NullBooleanField):
            return lambda x: generator.null_boolean()
        if isinstance(field, BinaryField):
            return lambda x: bytes
        if isinstance(field, CharField):
            if field.choices:
                return lambda x: generator.random_element(field.choices)[0]
            return lambda x: generator.text(field.max_length) if field.max_length >= 5 else generator.word()[
                                                                                            :field.max_length]
        if isinstance(field, DateField):
            return lambda x: generator.date()
        if isinstance(field, DateTimeField):
            return lambda x: generator.date_time(tzinfo=get_current_timezone())
        if isinstance(field, DecimalField):
            return lambda x: generator.pydecimal(rightDigits=field.decimal_places)
        if isinstance(field, DurationField):
            return lambda x: generator.time_delta()
        if isinstance(field, EmailField):
            return lambda x: generator.email()
        if isinstance(field, (FileField, FilePathField)):
            return lambda x: None
        if isinstance(field, FloatField):
            return lambda x: generator.pyfloat()
        if isinstance(field, (SmallIntegerField, PositiveSmallIntegerField)):
            return lambda x: generator.random_int(0, 65535)
        if isinstance(field, (IntegerField, PositiveIntegerField)):
            return lambda x: generator.random_int(0, 4294967295)
        if isinstance(field, BigIntegerField):
            return lambda x: generator.random_int(0, 18446744073709551615)
        if isinstance(field, ImageField):
            return lambda x: None
        if isinstance(field, (IPAddressField, GenericIPAddressField)):
            protocol_ip = generator.random_elements(['ipv4', 'ipv6'])
            return lambda x: getattr(generator, protocol_ip)()
        if isinstance(field, TextField):
            return lambda x: generator.text()
        if isinstance(field, TimeField):
            return lambda x: generator.time()
        if isinstance(field, URLField):
            return lambda x: generator.uri()
        if isinstance(field, SlugField):
            return lambda x: generator.slug()
        return lambda x: field.get_default()


class ModelPopulator(object):
    def __init__(self, model):
        """
        :param model: Generator
        """
        self.model = model
        self.field_formatters = {}

    def guess_field_formatters(self, generator):

        formatters = {}
        model = self.model
        name_guesser = Name(generator)
        field_type_guesser = FieldTypeGuesser(generator)

        for field in model._meta.fields + model._meta.many_to_many:
            #            yield field.name, getattr(self, field.name)
            field_name = field.name
            if isinstance(field, (ForeignKey, ManyToManyField, OneToOneField)):
                related_model = field.remote_field.model

                def build_relation(inserted):
                    if related_model in inserted and inserted[related_model]:
                        return related_model.objects.get(pk=random.choice(inserted[related_model]))
                    if not field.null:
                        try:
                            # try to retrieve random object from related_model
                            related_model.objects.order_by('?')[0]
                        except IndexError:
                            raise Exception(
                                'Relation "%s.%s" with "%s" cannot be null, check order of addEntity list' % (
                                    field.model.__name__, field.name, related_model.__name__,
                                ))
                    return None

                formatters[field_name] = build_relation
                continue

            if isinstance(field, AutoField):
                continue

            formatter = name_guesser.guess_format(field_name)
            if formatter:
                formatters[field_name] = formatter
                continue

            formatter = field_type_guesser.guess_format(field)
            if formatter:
                formatters[field_name] = formatter
                continue

        return formatters

    def execute(self, using, inserted_entities):

        obj = self.model()

        for field, formatter in self.field_formatters.items():
            if formatter:
                value = formatter(inserted_entities) if hasattr(formatter, '__call__') else formatter
                field_obj = obj._meta.get_field(field)
                if isinstance(field_obj, ManyToManyField):
                    obj.save(using=using)
                    getattr(obj, field).add(value)
                else:
                    setattr(obj, field, value)
        obj.save(using=using)

        return obj.pk


class Populator(object):

    def __init__(self, generator):
        """
        :param generator: Generator
        """
        self.generator = generator
        self.entities = {}
        self.quantities = {}
        self.orders = []

    def add_entity(self, model, number, custom_field_formatters=None):
        """
        Add an order for the generation of $number records for $entity.

        :param model: mixed A Django Model classname, or a faker.orm.django.EntityPopulator instance
        :type model: Model
        :param number: int The number of entities to populate
        :type number: integer
        :param custom_field_formatters: optional dict with field as key and callable as value
        :type custom_field_formatters: dict or None
        """
        if not isinstance(model, ModelPopulator):
            model = ModelPopulator(model)

        model.field_formatters = model.guess_field_formatters(self.generator)
        if custom_field_formatters:
            model.field_formatters.update(custom_field_formatters)

        klass = model.model
        self.entities[klass] = model
        self.quantities[klass] = number
        self.orders.append(klass)

    def execute(self, using=None):
        """
        Populate the database using all the Entity classes previously added.

        :param using A Django database connection name
        :rtype: A list of the inserted PKs
        """
        if not using:
            using = self.get_connection()

        inserted_entities = {}
        for klass in self.orders:
            number = self.quantities[klass]
            if klass not in inserted_entities:
                inserted_entities[klass] = []
            for i in range(0, number):
                inserted_entities[klass].append(self.entities[klass].execute(using, inserted_entities))

        return inserted_entities

    def get_connection(self):
        """
        use the first connection available
        :rtype: Connection
        """

        klass = self.entities.keys()
        if not klass:
            raise AttributeError('No class found from entities. Did you add entities to the Populator ?')
        klass = list(klass)[0]

        return klass.objects._db
