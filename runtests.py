#!/usr/bin/env python
import os
import sys

import django
from django.conf import settings
from django.test.utils import get_runner


def run_tests():
    klass = get_runner(settings)
    test_runner = klass(verbosity=1, interactive=True, failfast=False)
    failures = test_runner.run_tests(['tests'])
    return failures


if __name__ == '__main__':
    os.environ["DJANGO_SETTINGS_MODULE"] = "tests.test_settings"
    django.setup()
    sys.exit(bool(run_tests()))
